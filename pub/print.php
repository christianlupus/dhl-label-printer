<?php

$CONFIG = array();
require_once 'res/config.php';

global $loglines;
$loglines = array();

if(empty($_FILES['label']))
{
    header("Location: index.php");
    exit();
}

function returnMessage($msg)
{
    $uri = 'index.php?msg=' . urlencode($msg);
    header("Location: $uri");
    exit();
}

if($_FILES['label']['error'] != 0)
{
    // No label was given
    
    $msgs = array(
        1 => 'Der Wert upload_max_filesize in php.ini ist zu klein.',
        2 => 'Die PDF-Datei ist zu groß.',
        3 => 'Die Datei wurde nur teilweise übertragen.',
        4 => 'Keine PDF-Datei wurde angegeben.',
        6 => 'Kein temporäres Verzeichnis konnte gefunden werden.',
        7 => 'Konnte die Datei nicht speichern.',
        8 => 'Eine PHP-Erweiterung hat den Upload verhindert. Wenden Sie sich an den Administrator.'
    );
    
    $msg = $msgs[$_FILES['label']['error']];
    if(empty($msg))
        $msg = 'Interner Fehler gefunden: Fehlernummer ' . $_FILES['label']['error'];
    
    returnMessage($msg);
}

if($_FILES['label']['type'] !== 'application/pdf')
{
    returnMessage('Die Datei scheint keine PDF-Datei zu sein.');
}

function callCommand($cmd, $comment, $force = false)
{
    $output = array();
    $ret = 0;
    exec("$cmd 2>&1", $output, $ret);
    
    if($ret != 0 || $force)
    {
        global $loglines;
        $loglines[] = "$comment";
        foreach($output as $l)
            $loglines[] = "$l";
    }
    
    return $ret;
}

// Print additional info if needed
if(isset($_POST['extra']))
{
    $cmd = "lp -d {$CONFIG['printer']} -o InputSlot={$CONFIG['normal_slot']} " . escapeshellarg($_FILES['label']['tmp_name']);
    if(empty($CONFIG['debug']) || $CONFIG['debug'] == 0)
    {
        callCommand($cmd, 'Meldungen beim Drucken der Sendungsinformation: ');
    }
}

// Generate main info
$temp_file1 = tempnam(sys_get_temp_dir(), 'label');
callCommand("gs -o " . escapeshellarg($temp_file1) . " -sDEVICE=pdfwrite -g5950x4120 -dAutoRotatePages=/None -c '<</PageOffset [0 -430]>> setpagedevice' -f " . escapeshellarg($_FILES['label']['tmp_name']), 'Erste Wandlung: ');

$temp_file2 = tempnam(sys_get_temp_dir(), 'label');
callCommand("gs -o " . escapeshellarg($temp_file2) . " -sDEVICE=pdfwrite -g5950x8420 -dAutoRotatePages=/None -c '<</PageOffset [0 455]>> setpagedevice' -f " . escapeshellarg($temp_file1), 'Zweite Wandlung: ');
unlink($temp_file1);

$cmd = "lp -d {$CONFIG['printer']} -o InputSlot={$CONFIG['manual_slot']} " . escapeshellarg($temp_file2);
if(empty($CONFIG['debug']) || $CONFIG['debug'] == 0)
{
    callCommand($cmd, 'Meldungen beim Drucken des Etiketts: ');
}

unlink($temp_file2);

unlink($_FILES['label']['tmp_name']);

// print_r($_POST);
// print_r($_FILES);

?>
<!DOCTYPE html>
<html>
  <head>
    <meta charset="UTF-8">
    <title>DHL Etiketten Drucker</title>
  <link rel="stylesheet" href="style.css" type="text/css">
  </head>
  <body>
  <div class='col'></div>
  <div class='col center-col'>
      <div class='background'>
      
    	<form enctype="multipart/form-data" action="print.php" method="post">
    		<h1>DHL Versandetikett wird ausgedruckt</h1>
    		
            <div class='row'>Das Versandetikett muss nach oben im manuellen Schacht eingelegt werden, sodass das zu bedruckende Etiekess nahe beim Drucker ist.</div>
            <?php if(count($loglines) > 0): ?>
            	<div class='row msg'>
                  	Meldungen bei der Verarbeitung:
                  	<pre><?php echo join("\n", $loglines); ?></pre>
              	</div>
            <?php endif;?>
            <div class='row'>Ein <a href='index.php'>weiteres Etikett drucken</a>.</div>
    	</form>
      
      </div>
  </div>
  <div class='col'></div>
  </body>
</html>
