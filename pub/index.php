<!DOCTYPE html>
<html>
  <head>
    <meta charset="UTF-8">
    <title>DHL Etiketten Drucker</title>
  <link rel="stylesheet" href="style.css" type="text/css">
  <!--<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>-->
  </head>
  <body>
  <div class='col'></div>
  <div class='col center-col'>
      <div class='background'>
      
    	<form enctype="multipart/form-data" action="print.php" method="post">
    		<h1>DHL Versandetikett ausdrucken</h1>
    		
            <?php
            if(isset($_GET['msg'])):
            ?>
            <div class='error'><?php echo $_GET['msg']; ?></div>
            <?php 
            endif;
            ?>
    		
            <div class='row'>Versandetikett als PDF Datei: <input type="file" name="label" /></div>
            <div class='row'><input type="checkbox" name='extra' value='print' checked='checked'> Sendungsinformationen zus&auml;tzlich ausdrucken</div>
            
            <div class='row'><input type="submit" value="Absenden" /></div>
    	</form>
      
      </div>
  </div>
  <div class='col'></div>
  
  </body>
</html>
